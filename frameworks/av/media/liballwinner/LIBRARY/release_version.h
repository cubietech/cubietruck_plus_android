#ifndef CDX_RELEASE_VERSION_H
#define CDX_RELEASE_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

#define PLATFORM  "QIN"
#define LIBRARY_GIT_HEAD "d2a307593bca550a67dcb3e2071c1174cbe3422f"
#define LIBRARY_GIT_AUTHOR "hanming <hanming@allwinnertech.com>"
#define LIBRARY_GIT_DATE "Tue May 5 15:20:25 2015 +0800"
#define LIBRARY_GIT_RELEASE_AUTHOR "hanming"
#define LIBRARY_GIT_RELEASE_VERSION "V2.3.1"

static inline void ReleaseVersionInfo(void)
{

	logd("\n"
		">>>>>>>>>>>>>>>>>>>>>>>>>>>>>SDK RELEASE INFO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n" 
		"git release version: \t%s\n"
		"git commit: \t\t%s\n"
		"git last change date: \t%s\n"
		"git author: \t\t%s\n"
		"git release author: \t%s\n"
		"----------------------------------------------------------------------\n",
		LIBRARY_GIT_RELEASE_VERSION ,
		LIBRARY_GIT_HEAD,
		LIBRARY_GIT_DATE,
		LIBRARY_GIT_AUTHOR,
		LIBRARY_GIT_RELEASE_AUTHOR);
}

#ifdef __cplusplus
}
#endif

#endif

