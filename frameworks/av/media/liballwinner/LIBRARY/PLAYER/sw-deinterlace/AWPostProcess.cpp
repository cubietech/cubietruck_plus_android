
#define LOG_TAG "aw_pp"
// #define LOG_NDEBUG 0
#include <cutils/log.h>
#include <cutils/properties.h>
#include <string.h>
#include <stdio.h>
#include <sys/prctl.h>

#include "sys/resource.h"

#include "AWPostProcess.h"

#define F_LOG	// ALOGD("[%s], line: %d", __FUNCTION__, __LINE__);

static void* ppThread(void* arg);

static int64_t systemTime()
{
    struct timespec t;
    t.tv_sec = t.tv_nsec = 0;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec*1000000000LL + t.tv_nsec;
}

CAWPostProcess::CAWPostProcess()
{
	mInitWidth = 0;
	mInitHeight = 0;

	for(int i = 0; i < MAX_NUM_OF_THREAD; i++)
	{
		mPP[i] = NULL;
	}
	
	mInPicture = NULL;
	mOutPicture = NULL;
	mNumberOfThead = MAX_NUM_OF_THREAD;
	
	snprintf(m_propset, sizeof(m_propset), "%s%d:%d", "modem", getpid(), 2);
	property_set("media.boost.pref", m_propset);
	
	mMQStart = MessageQueueCreate(MAX_NUM_OF_THREAD, "ppMQStart");
	if (mMQStart == NULL)
	{
		ALOGE("MessageQueueCreate ppMQStart failed");
		return;
	}
	mMQDone = MessageQueueCreate(MAX_NUM_OF_THREAD, "ppMQDone");
	if (mMQDone == NULL)
	{
		ALOGE("MessageQueueCreate ppMQDone failed");
		return;
	}

	for (int i = 0; i < MAX_NUM_OF_THREAD; i++)
	{
		mThreadContext[i].pAWpp = this;
		mThreadContext[i].index = i;
		pthread_create(&mTid[i], NULL, ppThread, &mThreadContext[i]);
	}

#if DEBUG_STATISTIC
	mFrameCnt = 0;
	mTotalTime = 0;
#endif
	mInitOk = true;
}

CAWPostProcess::~CAWPostProcess()
{
	Message msg_start;
	for (int i = 0; i < MAX_NUM_OF_THREAD; i++)
	{
		msg_start.messageId = PP_MSG_EXIT;
		msg_start.params[0] = i;
		MessageQueuePostMessage(mMQStart, &msg_start);
	}

	for (int i = 0; i < MAX_NUM_OF_THREAD; i++)
	{
		pthread_join(mTid[i], NULL);
	}

    MessageQueueDestroy(mMQStart);
    MessageQueueDestroy(mMQDone);

	snprintf(m_propset,sizeof(m_propset),"%s%d:%d","moden",getpid(),2);
    property_set("media.boost.pref", m_propset);
	
#if DEBUG_STATISTIC
	if (mFrameCnt != 0)
	{
		ALOGD("###[star] average use time: %lld(us)", mTotalTime / mFrameCnt);
	}
#endif
	Dispose();
}

void CAWPostProcess::Dispose()
{
	for(int i = 0; i < MAX_NUM_OF_THREAD; i++)
	{
		if (mPP[i])
		{
			delete mPP[i];
			mPP[i] = NULL;
		}
	}
	mInitWidth = 0;
	mInitHeight = 0;
}

bool CAWPostProcess::CheckInit(int iWidth, int iHeight)
{
	AW_PP_TYPE type;
	if (iHeight > 720)
		type = PP_TYPE_CUBIC_INTERPOLATE;
	else
		type = PP_TYPE_YADIF;

	if(mInitWidth != iWidth || mInitHeight != iHeight)
	{
		if (mPP[0])
		{
			Dispose();
		}
		for (int i = 0; i < mNumberOfThead; i++)
		{
			mPP[i] = new CPostProcess();
			if (!mPP[i]->init(type, iWidth, iHeight))
			{
				goto error_out;
			}
		}		
		mInitWidth = iWidth;
		mInitHeight = iHeight;
	}
	return true;

error_out:
	for (int i = 0; i < mNumberOfThead; i++)
	{
		if (mPP[i] != NULL)
		{
			delete mPP[i];
			mPP[i] = NULL;
		}
	}
	return false;
}


bool CAWPostProcess::CheckWorkDone()
{
	bool work_done = true;
	for (int i = 0; i < mNumberOfThead; i++)
	{
		work_done = work_done && mPPDone[i];
	}

	return work_done;
}

bool CAWPostProcess::Process(VideoPicture* pPrePicture, VideoPicture* pInPicture, VideoPicture* pOutPicture, int nField)
{
	uint8_t * in_data[4];
	uint8_t * out_data[4];
	int in_lineSize[4];
	int out_lineSize[4];
	Message msg_start;
	Message msg_done;

	int64_t last_time = systemTime()/1000;

	if (!mInitOk)
	{
		ALOGE("aw pp do not init ok");
		return false;
	}

	if(pInPicture->ePixelFormat!= PIXEL_FORMAT_YV12)
	{
		ALOGE("aw pp format only support PIXEL_FORMAT_YV12, but in format is %d", pInPicture->ePixelFormat);
		return false;
	}

	if (pInPicture->nWidth > 640
		&& pInPicture->nHeight > 480)
	{
		mNumberOfThead = 4;
	}
	else
	{
		mNumberOfThead = 2;
	}

	if (mNumberOfThead > MAX_NUM_OF_THREAD)
	{
		mNumberOfThead = MAX_NUM_OF_THREAD;
	}

	if (!CheckInit(pInPicture->nWidth/mNumberOfThead, pInPicture->nHeight))
	{
		ALOGE("init aw pp failed");
		return false;
	}

	mPrePicture = pPrePicture;
	mInPicture = pInPicture;
	mOutPicture = pOutPicture;

	for (int i = 0; i < mNumberOfThead; i++)
	{
		msg_start.messageId = PP_MSG_START;
		msg_start.params[0] = i;
#ifdef DEBUG_STATISTIC
		msg_start.params[1] = mFrameCnt;
#endif
		MessageQueuePostMessage(mMQStart, &msg_start);
		
		mPPDone[i] = false;
	}
	
	do 
	{
		MessageQueueGetMessage(mMQDone, &msg_done);
		mPPDone[msg_done.params[0]] = 1;
		// ALOGD("-- thread %d done, %d", msg_done.params[0], CheckWorkDone());
	}while(!CheckWorkDone());

#if DEBUG_STATISTIC
	mFrameCnt++;
	mTotalTime += systemTime()/1000 - last_time;
#endif

	// ALOGD("!!! pp use: %lld", systemTime()/1000 - last_time);
	return true;
}

static void* ppThread(void* arg)
{
	int ret_val = 0;
	CAWPostProcess * pAWpp = ((THREAD_CONTEXT *)arg)->pAWpp;
	int threadId = ((THREAD_CONTEXT *)arg)->index;
	uint8_t * prev_data[4];
	uint8_t * in_data[4];
	uint8_t * out_data[4];
	int y_offset;
	int u_v_offset;
	int in_lineSize[4];
	int out_lineSize[4];
	VideoPicture *		pPrePicture;
	VideoPicture *		pInPicture;
	VideoPicture *		pOutPicture;
	Message msg_start;
	Message msg_done;
	int index = 0;

	char name[64];
	snprintf(name, sizeof(name), "pp_thread_%d", threadId);
	prctl(PR_SET_NAME, name);
	
	int status = setpriority(PRIO_PROCESS, gettid(), -3);
	ALOGV("ppThread setpriority return: %d", status);
	status = getpriority(PRIO_PROCESS, gettid());
	ALOGV("ppThread getpriority: %d", status);
	
	cpu_set_t mask;
	cpu_set_t get;
	CPU_ZERO(&mask);
	int cpu = (threadId % pAWpp->mNumberOfThead);
	CPU_SET(cpu, &mask); 
	if(sched_setaffinity(0, sizeof(mask), &mask) == -1)
	{
	   ALOGD("set affinity failed, cpu: %d, threadId: %d, pAWpp->mNumberOfThead: %d", cpu, threadId, pAWpp->mNumberOfThead);
	}
	else
	{
	   ALOGV("set affinity ok, cpu: %d, threadId: %d, pAWpp->mNumberOfThead: %d", cpu, threadId, pAWpp->mNumberOfThead);
	
		CPU_ZERO(&get);
		if (sched_getaffinity(0, sizeof(get), &get) == -1)
		{
			ALOGV("warning: cound not get thread affinity, continuing...");
		}
		else
		{
			int i;
			for (i = 0; i < pAWpp->mNumberOfThead; i++)
			{
				if (CPU_ISSET(i, &get))
				{
					ALOGV("thread %d is running on cpu %d", threadId,i);
				}
			}
		}
	}

	while(1)
	{
		if (MessageQueueGetMessage(pAWpp->mMQStart, &msg_start) != 0)
		{
			ALOGW("invalid mq");
			continue;
		}
		index = msg_start.params[0];
		// ALOGD("thread %d get mq index: %d, frame_cnt: %d", threadId, index, msg_start.params[1]);

		if (msg_start.messageId == PP_MSG_EXIT)
		{
			break;
		}

		if (pAWpp->mInPicture == NULL || pAWpp->mOutPicture == NULL)
		{
			ALOGE("error picture pointer");
			ret_val = -1;
			break;
		}

		pPrePicture = pAWpp->mPrePicture;
		pInPicture = pAWpp->mInPicture;
		pOutPicture = pAWpp->mOutPicture;

		// in para
		in_lineSize[0] = pInPicture->nLineStride;
		in_lineSize[1] = pInPicture->nLineStride / 2;
		in_lineSize[2] = pInPicture->nLineStride / 2;
	
		y_offset = index * pInPicture->nWidth / pAWpp->mNumberOfThead;
		u_v_offset = index * (pInPicture->nWidth / 2) / pAWpp->mNumberOfThead;

		prev_data[0] = (uint8_t *)pPrePicture->pData0 + y_offset;
		prev_data[1] = (uint8_t *)pPrePicture->pData1 + u_v_offset;
		prev_data[2] = (uint8_t *)pPrePicture->pData2 + u_v_offset;

		in_data[0] = (uint8_t *)pInPicture->pData0 + y_offset;
		in_data[1] = (uint8_t *)pInPicture->pData1 + u_v_offset;
		in_data[2] = (uint8_t *)pInPicture->pData2 + u_v_offset;
		
		// out para
		out_lineSize[0] = pOutPicture->nLineStride;
		out_lineSize[1] = pOutPicture->nLineStride / 2;
		out_lineSize[2] = pOutPicture->nLineStride / 2;
		
		out_data[0] = (uint8_t *)pOutPicture->pData0 + y_offset;
		out_data[1] = (uint8_t *)pOutPicture->pData1 + u_v_offset;
		out_data[2] = (uint8_t *)pOutPicture->pData2 + u_v_offset;

		pAWpp->mPP[index]->process((const uint8_t**)prev_data, (const uint8_t**)in_data, in_lineSize,
						out_data, out_lineSize);
		
		// work done
		msg_done.messageId = PP_MSG_DONE;
		msg_done.params[0] = index;
		MessageQueuePostMessage(pAWpp->mMQDone, &msg_done);

		// ALOGD("thread %d done index: %d", threadId, index);
	}

    pthread_exit(&ret_val);

	return NULL;
}

