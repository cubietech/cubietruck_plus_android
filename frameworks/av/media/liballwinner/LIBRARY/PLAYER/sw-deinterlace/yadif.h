#ifndef __YADIF_H__
#define __YADIF_H__

#include <string.h>
#include <stdio.h>

void yadif_plane(uint8_t *dst, int dst_stride,
    const uint8_t *prev0, const uint8_t *cur0, const uint8_t *next0, int refs,
    int w, int h, int parity, int tff);

#endif /* __YADIF_H__ */