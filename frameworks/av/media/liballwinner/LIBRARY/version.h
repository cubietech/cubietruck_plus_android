#ifndef CDX_VERSION_H
#define CDX_VERSION_H
#include "release_version.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAJOR_VERSION "2.3.1"
#define LIBRARY_GIT_REPOSITORY "git://xxxxx/qin2-stable"
#define LIBRARY_GIT_VERSION "AL3 commit	2b31fdf99b18a5b02d66ca81ae37c3379ee89836"
#define LIBRARY_GIT_TIME "Thu, 23 Apr 2015 16:42:02 +0800 (16:42 +0800)"
#define LIBRARY_RELEASE_AUTHOR "hankewei"

static inline void LogVersionInfo(void)
{
    logd("\n"
         ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> CedarX 2.0 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n" 
         "version: %s.%s\n"
         "svn repo:'%s'\n"
         "last change date: %s\n"
         "author: %s\n"
         "----------------------------------------------------------------------\n",
         MAJOR_VERSION, LIBRARY_GIT_VERSION,
         LIBRARY_GIT_REPOSITORY,
         LIBRARY_GIT_TIME,
         LIBRARY_RELEASE_AUTHOR); 
		 ReleaseVersionInfo(); 
}

/* usage: TagVersionInfo(myLibTag) */
#define TagVersionInfo(tag) \
    static void VersionInfo_##tag(void) __attribute__((constructor));\
    void VersionInfo_##tag(void) \
    { \
        logd("-------library tag: %s-------", #tag);\
        LogVersionInfo(); \
    }


#ifdef __cplusplus
}
#endif

#endif

