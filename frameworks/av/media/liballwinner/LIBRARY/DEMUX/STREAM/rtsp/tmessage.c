/*******************************************************************************
--                                                                            --
--                    CedarX Multimedia Framework                             --
--                                                                            --
--          the Multimedia Framework for Linux/Android System                 --
--                                                                            --
--       This software is confidential and proprietary and may be used        --
--        only as expressly authorized by a licensing agreement from          --
--                         Softwinner Products.                               --
--                                                                            --
--                   (C) COPYRIGHT 2011 SOFTWINNER PRODUCTS                   --
--                            ALL RIGHTS RESERVED                             --
--                                                                            --
--                 The entire notice above must be reproduced                 --
--                  on all copies and should not be removed.                  --
--                                                                            --
*******************************************************************************/

//#define LOG_NDEBUG 0
#define LOG_TAG "tmessage"
#include <CdxLog.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tmessage.h"

int message_create(message_queue_t* msg_queue)
{
	int 		i;
  
	CDX_LOGV("msg create\n");
	i = pthread_mutex_init(&msg_queue->mutex, NULL);
	if (i!=0)
	{
		return -1;
	}

	msg_queue->head = malloc(sizeof(message_t));
	if (!(msg_queue->head))
	{
		return -1;
	}

	memset(msg_queue->head, 0, sizeof(message_t));
	msg_queue->tail = msg_queue->head;

	msg_queue->tail->next = NULL;
	msg_queue->message_count = 0;

	return 0;
}


void message_destroy(message_queue_t* msg_queue)
{
	message_t* msg_tmp;

	pthread_mutex_lock(&msg_queue->mutex);

	msg_tmp = msg_queue->head;
	while(msg_tmp)
	{
		CDX_LOGV("msg destroy:%x : %x\n", msg_queue, msg_tmp);
		msg_queue->head = msg_queue->head->next;
		free(msg_tmp);
		msg_tmp = msg_queue->head;
		msg_queue->message_count--;
	}

	pthread_mutex_unlock(&msg_queue->mutex);
	pthread_mutex_destroy(&msg_queue->mutex);
}


void flush_message(message_queue_t* msg_queue)
{
	message_t* msg_tmp;

	pthread_mutex_lock(&msg_queue->mutex);

	msg_tmp = msg_queue->head;
	while(msg_tmp)
	{
		CDX_LOGV("msg destroy:%x : %x\n", msg_queue, msg_tmp);
		msg_queue->head = msg_queue->head->next;
		free(msg_tmp);
		msg_tmp = msg_queue->head;
		msg_queue->message_count--;
	}

	msg_queue->head = malloc(sizeof(message_t));
	if (!(msg_queue->head))
	{
		pthread_mutex_unlock(&msg_queue->mutex);
		return;
	}

	memset(msg_queue->head, 0, sizeof(message_t));
	msg_queue->tail = msg_queue->head;

	msg_queue->tail->next = NULL;
	msg_queue->message_count = 0;

	pthread_mutex_unlock(&msg_queue->mutex);
}

int put_message(message_queue_t* msg_queue, message_t *msg_in)
{
	pthread_mutex_lock(&msg_queue->mutex);

	if(msg_queue->message_count > MAX_MESSAGE_ELEMENTS)
	{
		CDX_LOGE("put message queue overflow ! %d vs %d", msg_queue->message_count, MAX_MESSAGE_ELEMENTS);
		pthread_mutex_unlock(&msg_queue->mutex);
		return -1;
	}
  
	message_t* new_msg;
	new_msg = malloc(sizeof(message_t));
	if (!(msg_queue->tail))
	{
		pthread_mutex_unlock(&msg_queue->mutex);
		free(new_msg);
		return -1;
	}

	CDX_LOGV("msg put:%x : %x\n", msg_queue, new_msg);
	memcpy(new_msg, msg_in, sizeof(message_t));

	msg_queue->tail->next = new_msg;
	msg_queue->tail 	  = msg_queue->tail->next;
	msg_queue->tail->next = NULL;

	msg_queue->message_count++;
	CDX_LOGV("put message count %d\n", msg_queue->message_count);
	pthread_mutex_unlock(&msg_queue->mutex);

	return 0;
}


int get_message(message_queue_t* msg_queue, message_t *msg_out)
{
	message_t *msg_tmp;

	pthread_mutex_lock(&msg_queue->mutex);
	if (msg_queue->message_count == 0)
	{
		pthread_mutex_unlock(&msg_queue->mutex);
		return -1;
	}

	memcpy(msg_out, msg_queue->head->next, sizeof(message_t));

	msg_tmp         = msg_queue->head;
	msg_queue->head = msg_queue->head->next;

	free(msg_tmp);
	CDX_LOGV("free message %p",msg_tmp);

	msg_queue->message_count--;
	pthread_mutex_unlock(&msg_queue->mutex);

	return 0;
}


int get_message_count(message_queue_t* msg_queue)
{
	int message_count;

	pthread_mutex_lock(&msg_queue->mutex);
	message_count = msg_queue->message_count;
	pthread_mutex_unlock(&msg_queue->mutex);

	return message_count;
}

//int show_message_head(message_queue_t* msg_queue, message_t *msg_out)
//{
//	message_t *msg_tmp;
//
//	pthread_mutex_lock(&msg_queue->mutex);
//	if (msg_queue->message_count == 0)
//	{
//		pthread_mutex_unlock(&msg_queue->mutex);
//		return -1;
//	}
//	memcpy(msg_out, msg_queue->head->next, sizeof(message_t));
//	pthread_mutex_unlock(&msg_queue->mutex);
//
//	return 0;
//}
//
//int show_message_tail(message_queue_t* msg_queue, message_t *msg_out)
//{
//	message_t *msg_tmp;
//
//	pthread_mutex_lock(&msg_queue->mutex);
//	if (msg_queue->message_count == 0)
//	{
//		pthread_mutex_unlock(&msg_queue->mutex);
//		return -1;
//	}
//	memcpy(msg_out, msg_queue->tail, sizeof(message_t));
//	pthread_mutex_unlock(&msg_queue->mutex);
//
//	return 0;
//}
