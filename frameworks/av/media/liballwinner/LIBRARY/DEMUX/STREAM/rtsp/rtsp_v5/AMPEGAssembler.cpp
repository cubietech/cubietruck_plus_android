/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_NDEBUG 0
#define LOG_TAG "AMPEGAssembler"
#include <utils/Log.h>

#include "AMPEGAssembler.h"

#include "ARTPSource.h"
#include "ASessionDescription.h"

#include <media/stagefright/foundation/ABitReader.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/hexdump.h>
#include <media/stagefright/Utils.h>

#include <ctype.h>
#include <stdint.h>


namespace android {

// static
AMPEGAssembler::AMPEGAssembler(
        const sp<AMessage> &notify, const AString &desc, const AString &params)
    : mNotifyMsg(notify),
      mParams(params),
      mAccessUnitRTPTime(0),
      mNextExpectedSeqNoValid(false),
      mNextExpectedSeqNo(0),
      mAccessUnitDamaged(false),
      mFirstBuffer(true),
      mIsMPEGVideo(false) {

	if(!strncasecmp("MPV/", desc.c_str(), 4)) {
		mIsMPEGVideo = true;
	}
}

AMPEGAssembler::~AMPEGAssembler() {

}

ARTPAssembler::AssemblyStatus AMPEGAssembler::addPacket(
        const sp<ARTPSource> &source) {
    List<sp<ABuffer> > *queue = source->queue();

    if (queue->empty()) {
        return NOT_ENOUGH_DATA;
    }

    if (mNextExpectedSeqNoValid) {
        List<sp<ABuffer> >::iterator it = queue->begin();
        while (it != queue->end()) {
            if ((uint32_t)(*it)->int32Data() >= mNextExpectedSeqNo) {
                break;
            }

            ALOGD("erase buffer %p", (*it).get());
            it = queue->erase(it);
        }

        if (queue->empty()) {
            return NOT_ENOUGH_DATA;
        }
    }

    sp<ABuffer> buffer = *queue->begin();

    if (!mNextExpectedSeqNoValid) {
        mNextExpectedSeqNoValid = true;
        mNextExpectedSeqNo = (uint32_t)buffer->int32Data();
    } else if ((uint32_t)buffer->int32Data() != mNextExpectedSeqNo) {
        ALOGV("Not the sequence number I expected");

        return WRONG_SEQUENCE_NUMBER;
    }

    uint32_t rtpTime;
    CHECK(buffer->meta()->findInt32("rtp-time", (int32_t *)&rtpTime));

    if (mPackets.size() > 0 && rtpTime != mAccessUnitRTPTime) {
        submitAccessUnit();
    }
    mAccessUnitRTPTime = rtpTime;

    size_t bufOffset = 0;
    if(mIsMPEGVideo) {
    	//Video header
    	/*
    	 *    0                   1                   2                   3
    	 *    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    	 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    	 *    | MBZ |T| TR | |N|S|B|E| P | | BFC | | FFC |
    	 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    	 *                 AN           FBV      FFV
    	*/


    	const uint8_t *data = buffer->data();
    	if((data[0] >> 3) != 0) {
    		ALOGD("video-specific header, MBZ %d", (data[0] >> 3));
    		return MALFORMED_PACKET;
    	}

    	int32_t extensionFlag = data[0] & 0x04;
    	if(mFirstBuffer) {
    		ALOGI("This is MPEG-%d video stream", extensionFlag +1);
    		mFirstBuffer = false;
    	}

    	int32_t activeNBytesFlag = data[2] & 0x80;
    	int32_t newHeaderFlag    = data[2] & 0x40;
    	int32_t seqHeaderFlag	 = data[2] & 0x20;
    	int32_t beginningOfSlice = data[2] & 0x10;
    	int32_t endOfSlice		 = data[2] & 0x08;
    	int32_t pictureType		 = data[2] & 0x07;

    	if(activeNBytesFlag == 1) {
    		if(!extensionFlag) {
    			//mpeg-1. AN MUST be 0
    			ALOGW("Header Error. video %d, AN %d", extensionFlag, activeNBytesFlag);
    			return MALFORMED_PACKET;
    		} else if(newHeaderFlag != 1) {
    			//mpeg-2. If AN = 1, the N MUST be 1
    			ALOGW("Header Error. AN %d, N %d",activeNBytesFlag, newHeaderFlag);
    			return MALFORMED_PACKET;
    		}
    	}

    	if(!pictureType) {
    		ALOGW("Header Error. Picture type %d forbidden", pictureType);
    		return MALFORMED_PACKET;
    	}

        bufOffset += 4;

    	if(extensionFlag) {
    		//parse MPEG-2 extension field
    		bufOffset += 4;
    		if(data[4] & 0x40) {
    			ALOGI("extension present, handle here");
    			//TODO:add extension field parse
    		}
    	}


    } else {
    	//Audio header
    	/*
    	 *  0 					1 					2 					3
    	 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    	 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    	 *  | MBZ 						  | 		Frag_offset           |
    	 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    	 */

        bufOffset += 4;
    }

    CHECK_GE(buffer->size(), bufOffset);

    buffer->setRange(buffer->offset() + bufOffset, buffer->size() - bufOffset);

    mPackets.push_back(buffer);

    queue->erase(queue->begin());
    ++mNextExpectedSeqNo;

    return OK;
}

void AMPEGAssembler::submitAccessUnit() {
    CHECK(!mPackets.empty());

//    ALOGV("Access unit complete (%d nal units)", mPackets.size());

    sp<ABuffer> accessUnit;

    //if (mIsGeneric) //for Android NuPlayer

    accessUnit = MakeCompoundFromPackets(mPackets);


    if (mAccessUnitDamaged) {
        accessUnit->meta()->setInt32("damaged", true);
    }

    mPackets.clear();
    mAccessUnitDamaged = false;

    uint32_t rtpTime;
    CHECK(accessUnit->meta()->findInt32(
                "rtp-time", (int32_t *)&rtpTime));

    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setBuffer("access-unit", accessUnit);
    msg->post();
}

ARTPAssembler::AssemblyStatus AMPEGAssembler::assembleMore(
        const sp<ARTPSource> &source) {
    AssemblyStatus status = addPacket(source);
    if (status == MALFORMED_PACKET) {
        mAccessUnitDamaged = true;
    }
    return status;
}

void AMPEGAssembler::packetLost() {
    CHECK(mNextExpectedSeqNoValid);
    ALOGV("packetLost (expected %d)", mNextExpectedSeqNo);

    ++mNextExpectedSeqNo;

    mAccessUnitDamaged = true;
}

void AMPEGAssembler::onByeReceived() {
    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setInt32("eos", true);
    msg->post();
}

}  // namespace android
