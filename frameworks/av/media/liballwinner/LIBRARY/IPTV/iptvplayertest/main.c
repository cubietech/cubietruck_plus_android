#include <stdio.h>
#include <stdlib.h>

extern int platform_init(int argc, char * argv[]);
extern void sw_player_test();
extern void sw_surface_exit();
extern void sw_player_set_surface(int playeridx, void* surface);

int main(int argc, char * argv[])
{
    platform_init(argc, argv);
	sw_surface_init();

	sw_player_set_surface(0, swget_VideoSurface());

    if (argc == 1)
       sw_player_test();
    //else
    //{
    //    extern void sw_player_mosaic_test(int display_width, int display_height, int count);
    //    sw_player_mosaic_test(1920, 1080, 20);
    //}
	
	sw_surface_exit();
	printf("media test exit!!!!!!\n");
    return 0;
}
