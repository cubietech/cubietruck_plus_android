LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS += -DLINUX -DANDROID

LOCAL_MODULE_TAGS := optional 

LOCAL_SRC_FILES:= \
	CTC_MediaProcessorWrapper.cpp \

LOCAL_C_INCLUDES := \
	$(TOP)/frameworks/av/include/media \
	$(LOCAL_PATH)/../../CTC_MediaProcessor  \
    $(LOCAL_PATH)/../../../CODEC/VIDEO/DECODER \
    $(LOCAL_PATH)/../../../CODEC/AUDIO/DECODER \
    $(LOCAL_PATH)/../../../CODEC/SUBTITLE/DECODER \
    $(LOCAL_PATH)/../../../PLAYER/ \
    $(LOCAL_PATH)/../../demux/ \
    $(LOCAL_PATH)/../include  \
	$(LOCAL_PATH)/../../include  

LOCAL_SHARED_LIBRARIES := \
	libplayer \
	libtvdemux 

LOCAL_MODULE := libCTC_MediaProcessorWrapper

include $(BUILD_STATIC_LIBRARY)
