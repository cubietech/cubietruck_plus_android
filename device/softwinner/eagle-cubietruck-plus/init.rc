import init.sun8i.usb.rc

import /init.environ.rc
import /init.usb.rc
import /init.${ro.hardware}.rc
import /init.trace.rc

on init
    # See storage config details at http://source.android.com/tech/storage/
    mkdir /mnt/shell 0700 shell shell
    mkdir /mnt/shell/emulated 0700 shell shell
    mkdir /storage 0751 root sdcard_r
    mkdir /storage/emulated 0555 root root

    export EXTERNAL_STORAGE /storage/emulated/legacy
    export EMULATED_STORAGE_SOURCE /mnt/shell/emulated
    export EMULATED_STORAGE_TARGET /storage/emulated
    export MEDIA_STORAGE /mnt/media/sdcard

    #Support legacy paths
    symlink /storage/emulated/legacy /sdcard
    symlink /storage/emulated/legacy /mnt/sdcard
    symlink /storage/emulated/legacy /storage/sdcard0
    symlink /mnt/shell/emulated/0 /storage/emulated/legacy

    mkdir /mnt/extsd 0700 system system
    symlink /mnt/extsd  /storage/extsd
    symlink /mnt/sdcard /storage/sdcard1

    mkdir /mnt/usbhost0 0700 system system
    symlink /mnt/usbhost0 /storage/usbhost0
    symlink /mnt/usbhost0 /storage/sdcard2
  
    mkdir /mnt/usbhost1 0700 system system
    symlink /mnt/usbhost1 /storage/usbhost1
    symlink /mnt/usbhost1 /storage/sdcard3
  
    mkdir /mnt/usbhost2 0700 system system
    symlink /mnt/usbhost2 /storage/usbhost2
  
    mkdir /mnt/usbhost3 0700 system system
    symlink /mnt/usbhost2 /storage/usbhost3
  
    mount debugfs debugfs /sys/kernel/debug
    mkdir /bootloader 0700 root system
    mkdir /databk 0700 root  system
  
    write  /sys/devices/platform/reg-22-cs-gpio1ldo/max_microvolts 2500000
    write  /sys/devices/platform/reg-22-cs-gpio1ldo/min_microvolts 2500000
    write  /sys/devices/platform/reg-15-cs-cldo1/max_microvolts    3000000
    write  /sys/devices/platform/reg-15-cs-cldo1/min_microvolts    3000000

    mount debugfs debugfs /sys/kernel/debug

on early-fs
    mkdir /mnt/Reserve0 0770 root root
    dispe2fsck
    mount_all /fstab.sun8i
on fs
    export PRIVATE_STORAGE /mnt/private
    format_userdata /dev/block/by-name/private PRIVATE
    mkdir /mnt/private 0000 system system
    mount vfat /dev/block/by-name/private /mnt/private
    exec /sbin/busybox chmod 0777 /dev/block/by-name/private
# radio related, such as imei.conf
    mkdir /data/misc/radio 0777 system radio

#gpu
    insmod /system/vendor/modules/pvrsrvkm.ko
    insmod /system/vendor/modules/dc_sunxi.ko

#csi module
    insmod /system/vendor/modules/videobuf-core.ko
    insmod /system/vendor/modules/videobuf-dma-contig.ko
    insmod /system/vendor/modules/uvcvideo.ko

#spdif
    insmod /system/vendor/modules/sunxi_spdif.ko
    insmod /system/vendor/modules/sunxi_spdma.ko
    insmod /system/vendor/modules/sndspdif.ko
    insmod /system/vendor/modules/sunxi_sndspdif.ko

#install ir driver . by Gary.
    insmod /system/vendor/modules/sunxi-ir-rx.ko

#gpio driver
    insmod /system/vendor/modules/gpio-sunxi.ko

on post-fs
    chown root system	/sys/devices/platform/pvrsrvkm/dvfs/android

service pvrsrvctl /system/vendor/bin/pvrsrvctl --start --no-module
	class core
	user root
	group root
	oneshot

on post-fs-data
    mkdir /data/media 0770 media_rw media_rw

on boot
    chmod 0777 /proc/driver/wifi-pm/power
# insmod network
    #insmod /system/vendor/modules/bcmdhd.ko
    insmod /system/vendor/modules/bt_sleep.ko

# bluetooth
    mkdir /data/misc/bluedroid 770 bluetooth net_bt_stack

    # power up/down interface
    chmod 0660 /sys/class/rfkill/rfkill0/state
    chmod 0660 /sys/class/rfkill/rfkill0/type
    chown bluetooth net_bt_stack /sys/class/rfkill/rfkill0/state
    chown bluetooth net_bt_stack /sys/class/rfkill/rfkill0/type
    write /sys/class/rfkill/rfkill0/state 0

    # bluetooth MAC address programming
    chown bluetooth net_bt_stack ro.bt.bdaddr_path
    chown bluetooth net_bt_stack /system/etc/bluetooth
    chown bluetooth net_bt_stack /data/misc/bluetooth
    setprop ro.bt.bdaddr_path "/data/misc/bluetooth/bdaddr"

    # bluetooth LPM
    chmod 0220 /proc/bluetooth/sleep/lpm
    chmod 0220 /proc/bluetooth/sleep/btwrite
    chown bluetooth net_bt_stack /proc/bluetooth/sleep/lpm
    chown bluetooth net_bt_stack /proc/bluetooth/sleep/btwrite

service btuartservice /system/bin/btuartservice
	class core
	user root
	group root
	disabled
	oneshot

on property:persist.service.bdroid.uart=*
    start btuartservice

# virtual sdcard daemon running as media_rw (1023)
service sdcard /system/bin/sdcard -u 1023 -g 1023 -l /data/media /mnt/shell/emulated
    class late_start

#service fuse_extsd /system/bin/sdcard -u 1023 -g 1023 -w 1023 -d /mnt/extsd /storage/extsd
#    class late_start
#    disabled

#service fuse_usbhost /system/bin/sdcard -u 1023 -g 1023 -w 1023 -d /mnt/usbhost /storage/usbhost
#    class late_start
#    disabled

# wifi service
# 1 wifi station and softap
service wpa_supplicant /system/bin/logwrapper /system/bin/wpa_supplicant \
    -iwlan0 -Dnl80211 -c/data/misc/wifi/wpa_supplicant.conf \
    -I/system/etc/wifi/wpa_supplicant_overlay.conf \
    -O/data/misc/wifi/sockets \
    -e/data/misc/wifi/entropy.bin -g@android:wpa_wlan0
    class main
    socket wpa_wlan0 dgram 660 wifi wifi
    disabled
    oneshot

# 2 wifi sta p2p concurrent service
service p2p_supplicant /system/bin/logwrapper /system/bin/wpa_supplicant \
    -iwlan0 -Dnl80211 -c/data/misc/wifi/wpa_supplicant.conf \
    -I/system/etc/wifi/wpa_supplicant_overlay.conf \
    -O/data/misc/wifi/sockets -N \
    -ip2p0 -Dnl80211 -c/data/misc/wifi/p2p_supplicant.conf \
    -I/system/etc/wifi/p2p_supplicant_overlay.conf \
    -puse_p2p_group_interface=1 -e/data/misc/wifi/entropy.bin \
    -g@android:wpa_wlan0
    class main
    socket wpa_wlan0 dgram 660 wifi wifi
    disabled
    oneshot
